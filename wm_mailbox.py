from imapclient import IMAPClient, exceptions
import time
from email.header import decode_header
import email
from bs4 import BeautifulSoup

class Mailbox:
    def __init__(self):
        self.user = ""
        self.pwd = ""
        self.server = ""
        self.port = 0
        self.encr = ""
        self.client = None

    def is_connected(self):
        if self.client is not None:
            return True
        return False

    def disconnect(self):
        if self.client is not None:
            self.client.logout()
        self.client = None

    def connect(self):
        self.disconnect()
        if self.port:
            client = IMAPClient(host=self.server, port=self.port)
        else:
            client = IMAPClient(host=self.server)
        try:
            client.login(self.user, self.pwd)
        except exceptions.LoginError:
            client = None
        if client is not None:
            self.client = client
            info = self.client.select_folder('INBOX')
            print info

    def get_uid(self):
        if self.client is None:
            return []
        messages = self.client.search('UNDELETED UNDRAFT')
        return messages

    def get_something(self, uid, stuffs=['ENVELOPE']):
        if self.client is None:
            return []
        r = self.client.fetch(uid, stuffs)
        return r.get(uid)

    def parse_body(self, body):
        parsedEmail = email.message_from_string(body)
        body = ""
        otherstuffs = []
        for part in parsedEmail.walk():
            if body != "" and part.get_content_maintype() == "text":
                otherstuffs.append(part.get_filename())
            elif body == "" and part.get_content_maintype() == "text":
                body = part.get_payload(decode=True)
                if part.get_content_type() == "text/html":
                    soup = BeautifulSoup(body, features="lxml")
                    for script in soup(["script", "style"]):
                        script.extract() 
                    text = soup.get_text()
                    # break into lines and remove leading and trailing space on each
                    lines = (line.strip() for line in text.splitlines())
                    # break multi-headlines into a line each
                    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
                    # drop blank lines
                    body = '\n'.join(chunk for chunk in chunks if chunk)
                    # # remove <!-- ... -->
                    # while "<!--" in body:
                    #     s1,s2 = body.split("<!--")[0],"<!--".join(body.split('<!--')[1:])
                    #     s2 = "-->".join(s2.split("-->")[1:])
                    #     body = s1+s2
                body = self.decode_body(body, part.get_charsets())
            elif part.get_content_maintype() in ["multipart", "image", "application"]:
                otherstuffs.append(part.get_filename())
        return body, otherstuffs

    def decode_body(self, body, charsets):
        if 0:
            print "---- body:"
            print body
            print "---- charsets:"
            print charsets
        try:
            if "us-ascii" in charsets:
                body2 = unicode(body, 'ascii')
                body = body2
            if "utf-8" in charsets:
                body2 = unicode(body, 'utf8')
                body = body2
            if "windows-1252" in charsets:
                body2 = unicode(body, 'cp1252')
                body = body2
        except TypeError:
            pass
        return body

    def decode(self, text):
        text = decode_header(text)[0]
        if 0:
            print text
        if text[1] == "utf-8":
            text = unicode(text[0], 'utf8')
        elif text[1] == "iso-8859-1":
            text = unicode(text[0], 'latin-1')            
        elif text[1] == "windows-1252":
            text = unicode(text[0], 'cp1252')
        else:
            text = unicode(text[0])
        return text

    def parse_subject(self, envelope):
        s = envelope.subject
        s = self.decode(s)
        return s

    def parse_name_from_adress(self, address):
        name = address.name
        if name is None:
            return None
        name = self.decode(name)
        if "Dr. " in name:
            name = name.replace("Dr. ","")
        if "," in name:
            name = name.replace(","," ")
        return name

    def parse_to(self, envelope):
        list1 = envelope.to
        list2 = envelope.cc
        list3 = envelope.bcc
        names = []
        for l in [list1, list2, list3]:
            if not l:
                continue
            for _ in l:
                name = self.parse_name_from_adress(_)
                if name and name not in names:
                    names.append(name)
        if names:
            names = ",".join(names)
            return names
        return ""

    def parse_from(self, envelope):
        list1 = envelope.from_
        list2 = envelope.reply_to
        names = []
        for l in [list1, list2]:
            if not l:
                continue
            for _ in l:
                name = self.parse_name_from_adress(_)
                if name and name not in names:
                    names.append(name)
        if names:
            names = ",".join(names)
            return names
        return ""

    def parse_date(self, envelope):
        dt = envelope.date
        timestamp = time.mktime(dt.timetuple())
        return int(timestamp)
