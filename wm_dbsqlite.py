#framework to access the db using sqlite3
#nothing related to tagliatelle are used here

import sqlite3
import os

class DbSqlite:
    def __init__(self,filename):
        self.filename = filename
        self.conn = None
        self.c = None
        self.table = os.path.split(filename)[-1].split(".")[0]#the table is the same has the name of the file
        if os.path.exists(filename):
            try:
                self.conn = sqlite3.connect(filename)
                self.c = self.conn.cursor()
            except:
                print ("Warning (db_sqlite):: impossible to load: %s"%filename)
                self.conn = None
        else:
            print ("Warning (db_sqlite):: filename not found: %s"%filename)

    def connect(self):
        self.conn = sqlite3.connect(self.filename)
        self.c = self.conn.cursor()

    def get_list(self,variable,additional=""):
        if self.conn is None:
            self.connect()
        rl = []
        r = self.c.execute('SELECT '+variable+' FROM '+self.table+additional)
        for rr in r:
            rl.append(rr)
        return rl

    def add(self, list_cols, list_values):
        if self.conn is None:
            self.connect()
        t1 = "("
        for l in list_cols:
            t1 += l+", "
        t1 = t1[:-2]+")"
        t2 = "("
        for l in list_values:
            # t2 += repr(l)+", "
            t2 += "?, "
        t2 = t2[:-2]+")"
        r = self.c.execute("INSERT INTO "+self.table+" "+t1+" VALUES "+t2, list_values)
        self.conn.commit()

    def create_table(self,variables,var_types):
        conn = sqlite3.connect(self.filename)
        table_name = self.table
        c = conn.cursor()

        dic_types = {"int":     "int",
                     "longint": "int",
                     "text":    "text",
                     "longtext":"text"}

        t = "( "
        for i in range(len(variables)):
            if i != 0: t+= " ,"
            t += variables[i]+" "+dic_types[var_types[i]]
        t += ")"
        print "CreateTable",table_name+t
        c.execute('''CREATE TABLE '''+table_name+t)
        conn.commit()
        conn.close()
        self.conn = sqlite3.connect(self.filename)
        self.c = self.conn.cursor()
