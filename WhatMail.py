#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2019 WhatMail
#   
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 2
#   of the License, or (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from PySide import QtCore, QtGui
import sys, os, time, random
from wm_mailbox import Mailbox
from wm_dbsqlite import DbSqlite
import _thread
import datetime

class MEl:
    def __init__(self):
        self.subject = ""
        self.from_ = ""
        self.to_ = ""
        self.tags = ""
        self.beginning = ""
        self.uid = 0
        self.date = "0"
        self.date_str = ""
        self.date_longstr = ""
        self.properties = ""
        self.content = ""

    def get_tags(self):
        return self.tags

    def get_from(self):
        return self.from_

    def get_to(self):
        return self.to_

    def get_content(self):
        return self.content

    def get_beginning(self):
        return self.beginning

    def get_subject(self):
        return self.subject

    def get_date_str(self):
        if self.date_str:
            return self.date_str
        else:
            dt = datetime.datetime.fromtimestamp(int(self.date))
            dtnow = datetime.datetime.now()
            days = ["Mo","Tu","We","Th","Fr","Sa","Su"]
            te = days[dt.weekday()]+" "+dt.strftime("%d")
            if (dtnow-dt).total_seconds() > 60*60*24*14:
                te += "/"+dt.strftime("%m")
            if (dtnow-dt).total_seconds() > 60*60*24*30:
                if dt.year != dtnow.year:
                    te += "/"+dt.strftime("%y")
            self.date_str = te
            return self.date_str

    def get_date_longstr(self):
        if self.date_longstr:
            return self.date_longstr
        else:
            dt = datetime.datetime.fromtimestamp(int(self.date))
            self.date_longstr = dt.strftime("%d/%m/%y %H:%M")
            return self.date_longstr

    def is_empty(self):
        if self.uid == 0:
            return True
        return False

class WhatMail(QtGui.QMainWindow):
    def __init__(self):
        super(WhatMail, self).__init__()

        self.images = {}
        self.load_images()
        
        self.main = QtGui.QWidget()
        self.setCentralWidget(self.main)
        #self.main.keyPressEvent = self.keyPressEvent

        self.mailbox = Mailbox()
        
        #1) toolbar

        self.buttons = []
        button_prop = {}
        # button_prop["do"] = [u'do','Update from server',0]
        # button_prop["up"] = [u'up','Transmit changes to server',0]
        
        self.bs = []
        # self.bs = ["do","up"]
        button_ico = []

        self.layout1 = QtGui.QHBoxLayout()
        self.layout1.setContentsMargins(0,0,0,0)
        #self.layout1.setSpacing(12)

        for i in range(len(self.bs)):
            p = button_prop[self.bs[i]]
            if self.bs[i]+".png" in self.images:
                self.buttons.append( QtGui.QPushButton('',self.main) )
                button_ico.append( QtGui.QIcon( self.images[self.bs[i]+".png"] ) )
                self.buttons[-1].setIcon(button_ico[-1])
                self.buttons[-1].setIconSize(QtCore.QSize(16,16))
            else:
                self.buttons.append( QtGui.QPushButton(p[0],self.main) )
            self.buttons[-1].setToolTip(p[1])
            self.buttons[-1].resize(24,24)
            #self.buttons[-1].setFlat(True)
            self.buttons[-1].setMaximumWidth(24)
            self.buttons[-1].setMinimumWidth(24)
            self.buttons[-1].setMaximumHeight(24)
            if p[2] == 1:
                self.buttons[-1].setCheckable(True)
            self.buttons[-1].clicked.connect(lambda i=i: self.buttonsClick(i))
            self.layout1.addWidget(self.buttons[-1], i+2)
        self.layout1.addStretch(100)

        self.layout2 = QtGui.QHBoxLayout()
        self.layout2.setContentsMargins(0,0,0,0)
        self.status_server = QtGui.QLabel("")
        self.status_server.setMaximumHeight(18)
        self.layout2.addWidget(self.status_server)
        self.status_action = QtGui.QLabel("")
        self.status_action.setMaximumHeight(18)
        self.layout2.addWidget(self.status_action)
        self.layout2.addStretch(100)

        #2) tab stuffs
        self.tabs = QtGui.QTabWidget()
        #policy1 = self.tabs.sizePolicy()
        #policy1.setHorizontalStretch(100)
        #policy1.setVerticalStretch(100)
        #self.tabs.setSizePolicy(policy1)
        #self.tabs.resize(5,5)
        
        #3) tab, config
        self.create_uitab_config()
    
        #4) tab, email
        self.create_uitab_email()
        self.view_happy = 0  # if view_happy = 0, try to update the view
        self.view_currentpos = 0

        self.tab_search = QtGui.QWidget()
        self.create_uitab_search()

        self.tabs.addTab(self.tab_email,"Emails")
        self.tabs.addTab(self.tab_config,"Config")
        self.tabs.addTab(self.tab_search,"Search")

        self.layoutf = QtGui.QVBoxLayout(self.main)
        if self.bs:
            self.layoutf.addLayout(self.layout1,1)
        self.layoutf.addWidget(self.tabs,2)
        self.layoutf.addLayout(self.layout2,1)
        
        #6) timer
        timer = QtCore.QTimer(self)
        self.connect(timer, QtCore.SIGNAL("timeout()"), self.time_update)
        timer.start(500)
        
        self.setWindowState(QtCore.Qt.WindowMaximized)

        self.actions = []
        self.actions.append("init_db")
        self.action_load_from_server = []
        self.action_delete_from_local = []
        self.actions.append("compare_with_server")
        self.selected = []
        self.selected_to_populate = []
        self.mels = {}
        self.actions.append("apply_selection")

        # timera = QtCore.QTimer(self)
        # self.connect(timera, QtCore.SIGNAL("timeout()"), self.do_action)
        # timera.start(1)   also remove the "while 1" in do_action 
        _thread.start_new_thread( self.do_action, () )

    def create_uitab_config(self):
        self.tab_config = QtGui.QWidget()

        default = {}
        try:
            with open("config.cfg") as file:  
                data = file.readlines()
                for d in data:
                    t = d.strip()
                    if ":" not in t:
                        continue
                    key, value = t.split(":")
                    key, value = key.strip(), value.strip()
                    if key == "pwd":
                        value = "".join([_ for i, _ in enumerate(value) if i%2 == 0])
                    default[key] = value
        except IOError:
            pass

        t1l0 = QtGui.QVBoxLayout()
        t1la1 = QtGui.QLabel("server:", self.tab_config)
        self.config_ui_server = QtGui.QLineEdit(self.tab_config)
        self.config_ui_server.setMinimumWidth(490)
        self.config_ui_server.setText(default.get("server",""))
        t1l1 = QtGui.QHBoxLayout()
        t1l1.addWidget(t1la1)
        t1l1.addWidget(self.config_ui_server)
        t1l1.addStretch(100)
        t1l0.addLayout(t1l1)

        t1la2 = QtGui.QLabel("user:", self.tab_config)
        self.config_ui_user = QtGui.QLineEdit(self.tab_config)
        self.config_ui_user.setMinimumWidth(500)
        self.config_ui_user.setText(default.get("user",""))
        t1l2 = QtGui.QHBoxLayout()
        t1l2.addWidget(t1la2)
        t1l2.addWidget(self.config_ui_user)
        t1l2.addStretch(100)
        t1l0.addLayout(t1l2)

        t1la3 = QtGui.QLabel("password:", self.tab_config)
        self.config_ui_pwd = QtGui.QLineEdit(self.tab_config)
        self.config_ui_pwd.setMinimumWidth(467)
        self.config_ui_pwd.setEchoMode(QtGui.QLineEdit.Password)
        ok = True
        default_text = default.get("pwd","")
        text, ok = QtGui.QInputDialog.getText(self.main, "Enter the mailbox password", "Password", QtGui.QLineEdit.Password, default_text, ok)
        if ok and text:
            self.config_ui_pwd.setText(text)
        t1l3 = QtGui.QHBoxLayout()
        t1l3.addWidget(t1la3)
        t1l3.addWidget(self.config_ui_pwd)
        t1l3.addStretch(100)
        t1l0.addLayout(t1l3)

        t1la4 = QtGui.QLabel("port:", self.tab_config)
        self.config_ui_port = QtGui.QLineEdit(self.tab_config)
        self.config_ui_port.setMinimumWidth(500)
        self.config_ui_port.setText(default.get("port",""))
        t1l4 = QtGui.QHBoxLayout()
        t1l4.addWidget(t1la4)
        t1l4.addWidget(self.config_ui_port)
        t1l4.addStretch(100)
        t1l0.addLayout(t1l4)

        t1la5 = QtGui.QLabel("encryption:", self.tab_config)
        self.config_ui_encr = QtGui.QLineEdit(self.tab_config)
        self.config_ui_encr.setMinimumWidth(463)
        self.config_ui_encr.setText(default.get("encr",""))
        t1l5 = QtGui.QHBoxLayout()
        t1l5.addWidget(t1la5)
        t1l5.addWidget(self.config_ui_encr)
        t1l5.addStretch(100)
        t1l0.addLayout(t1l5)

        self.config_ui_button = QtGui.QPushButton("Apply", self.tab_config)
        self.config_ui_button.clicked.connect(self.init_mailbox)
        t1l0.addWidget(self.config_ui_button)

        self.init_mailbox()

        t1l0.addStretch(100)
        self.tab_config.setLayout(t1l0)

    def create_uitab_search(self):
        self.tab_search = QtGui.QWidget()

        t1l0 = QtGui.QVBoxLayout()

        self.search_ui_cb1 = QtGui.QComboBox(self.tab_search)
        self.search_ui_cb1.setMinimumWidth(490)
        self.search_ui_cb1.setEditable(True)
        t1l0.addWidget(self.search_ui_cb1)
        
        self.search_ui_button = QtGui.QPushButton("Search", self.tab_search)
        # self.search_ui_button.clicked.connect(self.init_mailbox)
        t1l0.addWidget(self.search_ui_button)

        t1l0.addStretch(100)
        self.tab_search.setLayout(t1l0)

    def create_uitab_email(self):
        self.tab_email = QtGui.QWidget()
        scene0 = QtGui.QGraphicsScene()
        self.tagbar = QtGui.QGraphicsView(scene0,self.tab_email)
        self.tagbar.setStyleSheet("background-color : grey; border:none;");
        policy1 = self.tagbar.sizePolicy()
        policy1.setHorizontalStretch(90)
        policy1.setVerticalStretch(1)
        self.tagbar.setSizePolicy(policy1)
        self.tagbar.resize(2000,80)
        #self.view.mousePressEvent = self.mousePressEvent_view
        #self.view.wheelEvent = self.wheelEvent_view
        #self.view.setMouseTracking(True)
        #self.scene.mousePressEvent = self.mousePressEvent_view
        self.tagbar.show()
        self.scene = QtGui.QGraphicsScene()
        self.view = QtGui.QGraphicsView(self.scene,self.tab_email)
        self.view.setStyleSheet("background-color : whitesmoke; border:none;");
        policy1 = self.view.sizePolicy()
        policy1.setHorizontalStretch(90)
        policy1.setVerticalStretch(90)
        self.view.setSizePolicy(policy1)
        self.view.resize(2000,3000)
        self.view.setMouseTracking(True)
        self.view.mouseMoveEvent = self.mouseMoveEvent_view
        #self.view.mousePressEvent = self.mousePressEvent_view
        #self.view.wheelEvent = self.wheelEvent_view
        #self.scene.mousePressEvent = self.mousePressEvent_view
        self.view.show()
        self.colorbar = QtGui.QLabel("")
        self.colorbar_image = None #QtGui.QImage(10,200,QtGui.QImage.Format_ARGB32)
        self.colorbar_painter = None
        self.colorbar.setFixedWidth(24)
        policy4 = self.view.sizePolicy()
        policy4.setHorizontalStretch(90)
        self.colorbar.setSizePolicy(policy4)
        #self.colorbar.mousePressEvent = self.mousePressEvent_colorbar
        self.colorbar.setStyleSheet("background-color : black; border:none;");
        #self.colorbar.setPixmap(QtGui.QPixmap(self.colorbar_image))
        self.scrollbar_v = QtGui.QScrollBar(self.main,QtCore.Qt.Vertical)
        policy3 = self.view.sizePolicy()
        policy3.setHorizontalStretch(1)
        self.scrollbar_v.setSizePolicy(policy3)
        self.scrollbar_v.setMaximumWidth(24)
        self.scrollbar_v.setMinimumWidth(24)
        self.scrollbar_v.show()
        self.scrollbar_v.dont_change = 0
        #self.scrollbar_v.time = time.time()
        self.scrollbar_v.connect(self.scrollbar_v, QtCore.SIGNAL("valueChanged(int)"), self.scrollbar_v_moved)

        self.create_label_emails()

        t2l0 = QtGui.QHBoxLayout()
        t2l0.setContentsMargins(0,0,0,0)
        t2l1 = QtGui.QVBoxLayout()
        t2l1.setContentsMargins(0,0,0,0)
        t2l1.addWidget(self.tagbar)
        t2l1.addWidget(self.view)
        t2l0.addLayout(t2l1)
        t2l0.addWidget(self.colorbar)
        t2l0.addWidget(self.scrollbar_v)
        self.tab_email.setLayout(t2l0)

    def mouseMoveEvent_view(self, event):
        if event.x() < 400 or event.x() > 500:
            self.view_labels[-1].move(10,-10000)
            return
        i = int((event.y() - 1)/20)
        y = 20*(i+1)
        la = self.view_labels[-1]
        uid = 0
        los = []
        try:
            uid = self.view_labels[i].uid
            #los = self.view_labels[i].list_of_size
            #print los
            mel = self.mels[uid]
        except (KeyError, AttributeError):
            uid = 0
            mel = MEl()
            y = -10000
        t = "<html><body>"
        t += "<font color='grey' size=2> Subject: </font><font size=3>%s</font><br \>"%mel.get_subject()
        t += "<font color='grey' size=2> Date: </font><font size=3>%s</font><br \>"%mel.get_date_longstr()
        t += "<font color='grey' size=2> From: </font><font size=3>%s</font><br \>"%mel.get_from()
        t += "<font color='grey' size=2> To: </font><font size=3>%s</font><br \>"%mel.get_to()
        t += "<font color='grey' size=2> Content: </font><font size=3>"
        content = mel.get_content()
        while len(content) > 150:
            t += content[:150]
            content = content[150:]
            for j in range(50):
                t += content[:1]
                content = content[1:]
                if not content or t[-1] == " ":
                    break
            t += "<br \>"
        t += content+"<br \>"
        t += "</font>"
        t += "</body></html>"
        la.setText(t)
        la.adjustSize()        
        if i > 5:
            # show on top
            yt = y -20 - la.height()
            if yt >= 0:
                y = yt
        la.move(1,y)

    def load_images(self):
        self.images = {}
        mypath = os.path.join(os.path.dirname(os.path.realpath(__file__)),"img")
        onlypngfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f)) and f.endswith(".png")]
        for fn in onlypngfiles:
            self.images[fn] = QtGui.QPixmap( os.path.join(mypath, fn))

    def set_status_server(self, text):
        self.status_server.setText("")
        key = "server_%s.png"%text
        if key in self.images:
            self.status_server.setPixmap(self.images[key])
        self.status_server.setToolTip(text)

    def time_update(self):
        # check status
        if self.mailbox.is_connected():
            self.set_status_server("connected")
        else:
            self.set_status_server("disconnected")
        # check if we are happy with the view
        if self.view_happy != 1:
            self.view_update()

    def get_color_from_text(self,t):
        i = hash(t)
        r = (i%255)
        g = (int(i/1000)%255)
        b = (int(i/1000000)%255)
        if r+g+b > 600:
            if r > 200:
                r += -100
            if g > 200:
                g += -100
            if b > 100:
                b += -100
        if r+g+b < 300:
            if r < 100:
                r += 100
            if g < 100:
                g += 100
            if b < 100:
                b += 100
        return '#%02x%02x%02x' % (r,g,b)

    def find_where_clicked(self, mel, x):
        tags = mel.get_tags()
        date = mel.get_date_str()
        from_ = mel.get_from()
        subject = mel.get_subject()
        tla = QtGui.QLabel()
        ta = "<html><body>"
        tb = "</body></html>"
        t = ""+ta
        for l in tags.split(","):
            cl = self.get_color_from_text(l)
            t1 = "<font color=%s size=3><b>%s</b></font>"%(cl,"&#9679;")
            t += t1
            tla.setText(t+tb)
            tla.adjustSize()
            w = tla.width()
            if x < w:
                return "tag", l
        t1 = "<font size=1> %s</font><font size=3>&nbsp;-&nbsp;</font>"%date
        t += t1
        tla.setText(t+tb)
        tla.adjustSize()
        w = tla.width()
        if x < w:
            return "date", date
        for i, l in enumerate(from_.split(",")):
            if i > 0: 
                t1 = "<font size=2>,</font>"
                t += t1
            cl = self.get_color_from_text(l)
            t1 = "<font color=%s size=2>%s</font>"%(cl,l)
            t += t1
            tla.setText(t+tb)
            tla.adjustSize()
            w = tla.width()
            if x < w:
                return "from", l
        t1 = "<font size=3>&nbsp;- %s</font>"%subject
        t += t1
        tla.setText(t+tb)
        tla.adjustSize()
        w = tla.width()
        if x < w:
            return "subject", subject
        t += "<font size=1 color='grey'>&nbsp;&nbsp;%s</font>"%mel.get_beginning()
        t += tb
        return "beginning", ""

    def get_text_for_data(self, mel):
        tb = "<html><body>"
        te = "</body></html>"

        if mel.is_empty():
            b = "&#9644;"
            t_date = tb+"<font color='lightgrey' size=3>%s</font>"%(b*4)+te
            t_sender = tb+"<font color='lightgrey' size=3>%s</font>"%(b*12)+te
            t = "<font color='lightgrey' size=3>"
            for _ in range(3+random.randint(0,3)):
                i = random.randint(5,15)
                t += (b*i)+" "
            t += "</font>"
            t_title = tb+t+te
            t = "</font><font color='#e9e9e9' size=3>"
            for _ in range(15):
                i = random.randint(5,15)
                t += (b*i)+" "
            t += "</font>"
            t_content = tb+t+te
            return t_date, t_sender, t_title, t_content

        tags = mel.get_tags()
        date = mel.get_date_str()
        from_ = mel.get_from()
        subject = mel.get_subject()
        t_tag = ""
        for l in tags.split(","):
            cl = self.get_color_from_text(l)
            t1 = "<font color=%s size=3><b>%s</b></font>"%(cl,"&#9632;")  # &#9679;")
            t_tag += t1
        t_date = "<font size=1> %s</font><font size=3>&nbsp;-&nbsp;</font>"%date
        t_date = tb+t_date+te
        t = ""
        for i, l in enumerate(from_.split(",")):
            if i > 0: 
                t1 = "<font size=2>,</font>"
                t += t1
            cl = self.get_color_from_text(l)
            t1 = "<font color=%s size=2>%s</font>"%(cl,l)
            t += t1
        t_sender = tb+t+te
        t_title = "<font size=3>&nbsp;- %s</font>"%subject
        t_title = tb+t_title+te
        
        t_content = "<font size=1 color='grey'>&nbsp;&nbsp;%s</font>"%mel.get_beginning()
        t_content = tb+t_content+te
        return t_date, t_sender, t_title, t_content

    def view_update(self):
        i0 = self.view_currentpos
        max_i = len(self.view_labels)-1
        selected = self.selected[i0:i0+max_i]
        colors = ['#e3e3e3', '#f7f7f7']
        for i, sel in enumerate(selected):
            if sel not in self.mels:                
                self.view_happy = 0
                break
            la = self.view_labels[i]
            la.setStyleSheet("QLabel { background-color :%s ; }"%colors[(i+i0)%2])
            mel = self.mels[sel]
            t_date, t_sender, t_title, t_content = self.get_text_for_data(mel)
            la.l_date.setText(t_date)
            la.l_sender.setText(t_sender)
            la.l_title.setText(t_title)
            la.l_content.setText(t_content)
            #la.adjustSize()
            la.uid = mel.uid
        else:
            self.view_happy = 1
        self.scrollbar_v.dont_change = 1
        self.scrollbar_v.setMaximum(len(self.selected))
        self.scrollbar_v.setMinimum(0)
        self.scrollbar_v.setValue(self.view_currentpos)
        self.scrollbar_v.dont_change = 0

    def create_label_emails(self):
        self.view_labels = []
        y = 0
        desk = QtGui.QDesktopWidget()
        y_max = desk.availableGeometry().height()-60
        empty = MEl()
        colors = ['#e3e3e3', '#f7f7f7']
        while y < y_max:
            t_date, t_sender, t_title, t_content = self.get_text_for_data(empty)
            lo1 = QtGui.QFrame(self.view)
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0,0,0,0)
            layout.addStretch()
            lo1.l_date = QtGui.QLabel(t_date)
            layout.addWidget(lo1.l_date)
            lo1.l_sender = QtGui.QLabel(t_sender)
            layout.addWidget(lo1.l_sender)
            lo1.l_title = QtGui.QLabel(t_title)
            layout.addWidget(lo1.l_title)
            lo1.l_content = QtGui.QLabel(t_content)
            layout.addWidget(lo1.l_content)
            layout.addStretch(100)
            layout.setSpacing(0)
            lo1.setLayout(layout)

            lo1.l_sender.adjustSize()
            lo1.l_date.adjustSize()
            lo1.adjustSize()
            lo1.show()
            lo1.move(1,1+y)
            y+=20
            lo1.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents)
            #lo1.setMouseTracking(True)
            #lo1.mouseMoveEvent = self.mouseMoveEvent_view
            # lo2.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
            # lo2.wheelEvent = self.wheelEvent_view
            # lo2.setMouseTracking(True)
            lo1.uid = 0
            lo1.setStyleSheet("QFrame { background-color :%s ; }"%colors[len(self.view_labels)%2])
            self.view_labels.append(lo1)

        lo1 = QtGui.QLabel("floating guy\ntwolines",self.view)
        lo1.setStyleSheet("QLabel { background-color :'white' ; }")
        lo1.show()
        lo1.move(1,1+y)
        lo1.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents)
        self.view_labels.append(lo1)

    def scrollbar_v_moved(self,changed):
        if self.scrollbar_v.dont_change:
            return
        self.view_currentpos = self.scrollbar_v.sliderPosition()
        self.view_update()

    def do_action(self):
        # this guy has its own thread, and do everything in there
        while 1:
            current_action = ""
            if not self.actions:
                time.sleep(0.1)
            else:
                current_action = self.actions[0]
                self.actions = self.actions[1:]
            if current_action == "init_db":
                self.init_db()
            elif current_action == "compare_with_server":
                self.compare_with_server()
            elif current_action == "load_from_server":
                self.load_from_server()
            elif current_action == "delete_from_local":
                self.delete_from_local()
            elif current_action == "apply_selection":
                self.apply_selection()
            elif current_action == "populate_mel":
                self.populate_mel()

    def apply_selection(self):
        list_local_uid = self.db_mail.get_list("uid"," ORDER BY date DESC")
        list_local_uid = [_[0] for _ in list_local_uid]
        if list_local_uid != self.selected:
            self.selected = list_local_uid
            self.selected_to_populate = list_local_uid[:]
            self.actions.append("populate_mel")
        
    def populate_mel(self):
        if not self.selected_to_populate:
            return
        uid = self.selected_to_populate[0]
        self.selected_to_populate = self.selected_to_populate[1:]
        self.set_status_action("Loading selected email (%i remaining)"%(len(self.selected_to_populate)+1))
        if uid in self.mels.keys():
            self.actions.append("populate_mel")
            return
        mel = MEl()
        mel.uid = uid
        cols = "subject, from_, to_, date, tags, properties, beginning"
        result = self.db_mail.get_list(cols," WHERE uid = %i"%uid)
        result = result[0]
        for i, col in enumerate(cols.split(",")):
            column = col.strip()
            setattr(mel, column, result[i])
        cols =  "content" #, attachmentfilenames"
        result = self.db_body.get_list(cols," WHERE uid = %i"%uid)
        result = result[0]
        for i, col in enumerate(cols.split(",")):
            column = col.strip()
            setattr(mel, column, result[i])
        self.mels[uid] = mel
        self.actions.append("populate_mel")

    def set_status_action(self,text):
        self.status_action.setText(text)

    def compare_with_server(self):
        list_remote_uid = self.mailbox.get_uid()
        list_local_uid = self.db_mail.get_list("uid")
        list_local_uid = [_[0] for _ in list_local_uid]
        print "Remote uid:",len(list_remote_uid)
        print "Local uid:",len(list_local_uid)
        remaining = [_ for _ in list_remote_uid if _ not in list_local_uid]
        print "Remaining",len(remaining)
        deleted = [_ for _ in list_local_uid if _ not in list_remote_uid]
        print "Deleted",len(deleted)
        l = self.action_load_from_server + remaining  
        self.action_load_from_server = list(set(l))
        l = self.action_delete_from_local + deleted
        self.action_delete_from_local = list(set(l))
        self.actions.append("load_from_server")
        self.actions.append("delete_from_local")

    def load_from_server(self):
        if not self.action_load_from_server:
            return
        n = len(self.action_load_from_server)
        uid = self.action_load_from_server[0]
        self.action_load_from_server = self.action_load_from_server[1:]
        self.set_status_action("Download from server (%i remaining)"%n)
        dico = self.mailbox.get_something(uid, ['ENVELOPE', 'RFC822'])
        if dico is None:
            dico = {}
        self.add_to_db_body(uid, dico.get("RFC822",None))
        self.add_to_db_mail(uid, dico.get("ENVELOPE",None))
        self.actions.append("load_from_server")

    def delete_from_local(self):
        if not self.action_delete_from_local:
            return
        n = len(self.action_delete_from_local)
        uid = self.action_delete_from_local[0]
        self.action_delete_from_local = self.action_delete_from_local[1:]
        self.set_status_action("Delete from local (%i remaining)"%n)

    def add_to_db_mail(self, uid, envelope):
        if envelope is None:
            return
        subject = self.mailbox.parse_subject(envelope)
        from_ = self.mailbox.parse_from(envelope)
        to_ = self.mailbox.parse_to(envelope)
        date = self.mailbox.parse_date(envelope)
        result = self.db_body.get_list("content, attachmentfilenames"," WHERE uid = %i"%uid)
        beginning = result[0][0].replace("\n","")[:250]
        attachment = result[0][1]
        prop = ["new"]
        attach_value = 0
        for _ in attachment.split(","):
            if not _:
                continue
            if _[-4:] in [".png", ".gif", ".jpg", "jpeg"]:
                attach_value += 1
            else:
                attach_value += 5
        if attach_value >= 4:
            prop.append("attach")
        cols =  ["uid", "subject", "from_","to_", "date",   "tags", "properties",  "beginning"]
        values = [uid,  subject,   from_,  to_,   date,     "",     ",".join(prop), beginning]
        self.db_mail.add(cols, values)

    def add_to_db_body(self, uid, body):
        if body is None:
            return ""
        body,attachment = self.mailbox.parse_body(body)
        attachment = "".join([_ for _ in attachment if _])
        cols =  ["uid", "content", "attachmentfilenames"]
        values = [uid, body, attachment]
        # print "------------- bodyWM:"
        # print body
        # print "-------------"
        self.db_body.add(cols, values)

    def init_db(self):
        self.db_mail = DbSqlite("db/mail.sqlite")
        self.db_tags = DbSqlite("db/tags.sqlite")
        self.db_body = DbSqlite("db/body.sqlite")
        if self.db_mail.conn is None:
            cols =  ["uid", "subject", "from_","to_", "date",   "tags", "properties", "beginning"]
            types = ["int", "text",    "text", "text", "longint","text", "text",      "text"]
            self.db_mail.create_table(cols, types)
        if self.db_tags.conn is None:
            cols =  ["tid", "name", "description", "properties"]
            types = ["text","text", "text",        "text"]
            self.db_tags.create_table(cols, types)
        if self.db_body.conn is None:
            cols =  ["uid", "content", "attachmentfilenames"]
            types = ["int", "longtext","text"]
            self.db_body.create_table(cols, types)
    
    def init_mailbox(self):
        self.mailbox.server = self.config_ui_server.text()
        self.mailbox.user = self.config_ui_user.text()
        self.mailbox.pwd = self.config_ui_pwd.text()
        self.mailbox.port = int(self.config_ui_port.text())
        self.mailbox.encr = self.config_ui_encr.text()
        self.mailbox.disconnect()
        self.set_status_server("connecting")
        self.mailbox.connect()





        
    def keyPressEvent(self,t):
        #if t.key()==QtCore.Qt.Key_Return:
        #    self.listViewClick(True)
        #if t.key()==QtCore.Qt.Key_Up or t.key()==QtCore.Qt.Key_Down:
        #    self.listViewClick(False)
        if t.key()==QtCore.Qt.Key_Escape:
            self.close()
        if t.key()==QtCore.Qt.Key_W:
            self.close()
        
    def buttonsClick(self,i):
        global history,currentPath,currentFile
        if self.bs[i] == "ti":
            print "ti"
        elif self.bs[i] in ["cf","cl"]:
            if self.currentDialog:
                pass#a dialog is open
            else:
                if self.bs[i] == "cl":
                    self.currentDialog = ColDlg(self)
                if self.bs[i] == "cf":
                    self.currentDialog = CfgDlg(self)
                self.currentDialog.run()
            
                            
    def display(self):
        total = len(self.lines)
        max_length = 0
        i = 0
        j = self.view_current+0
        self.view_clickzones = {}
        #while self.lines[j].visible == 0:
        #    bci = self.lines[j].box_contained_in
        #    if bci == -1:
        #        break
        #    j = bci
        x0 = self.view_x
        first = j+0
        while i < len(self.view_labels):
            la1 = self.view_labels[i][0]
            la2 = self.view_labels[i][1]
            i += 1
            if j >= len(self.lines):
                l = None
            else:
                l = self.lines[j]
            while l is not None and l.visible == 0:
                j += 1
                if j >= len(self.lines):
                    l = None
                else:
                    l = self.lines[j]
            if l is None:
                la1.setText("")
                la2.setText("")
                la1.setStyleSheet("")
                la2.setStyleSheet("")                
                continue
            t = "<html><body>"
            t += "<tt>"
            
            lns = 1+int(math.log10(total))
            ln = str(l.number+1).zfill(lns)
            if (l.number+1)%10 == 0:
                t += "<font color='magenta' size=1>%s </font>"%ln
            else:
                t += "<font color='grey' size=1>%s </font>"%ln
            ts = "&#9474;"
            cl = "black"
            t += "<font color='%s'>%s </font>"%(cl,ts)
            t += "</tt>"
            t += "</body></html>"
            la1.move(1+x0,la1.pos().y())
            la1.setText(t)
            la1.adjustSize()
            la1.linenumber = l.number
            self.view_last = l.number
            la2.move(1+x0+la1.width(),la2.pos().y())
            t = "<html><body>"
            t += "<tt>"
            t += l.clean
            t += "</tt>"
            t += "</body></html>"
            la2.setText(t)
            la1.setStyleSheet("")
            la2.setStyleSheet("")
            if l.colors.get("body") and l.colors.get("bkgd"):
                la2.setStyleSheet("QLabel { background-color :"+l.colors.get("bkgd")+" ; color : "+l.colors.get("body")+"; }")
            elif l.colors.get("body"):
                la2.setStyleSheet("QLabel { color : "+l.colors.get("body")+"; }")
            elif l.colors.get("bkgd"):
                la2.setStyleSheet("QLabel { background-color :"+l.colors.get("bkgd")+" ; }")
            if l.colors.get("head"):
                la1.setStyleSheet("QLabel { background-color :"+l.colors.get("head")+" ; }")
            la2.adjustSize()
            if la1.width()+la2.width() > max_length:
                max_length = la1.width()+la2.width()
            if 0:
                x,y = la1.pos().x(),la1.pos().y()
                self.view_clickzones["(%i,%i)"%(x+27,y+10)] = ["arrow",l]
            j += 1
        self.view_current = first
        self.update_position_colorbar()
        self.scrollbar_v.dont_change = 1
        self.scrollbar_v.setMaximum(len(self.scrollbar_v.lines))
        self.scrollbar_v.setMinimum(0)
        self.scrollbar_v.setValue(self.scrollbar_v.lines.index(first))
        self.scrollbar_v.dont_change = 0
        self.scrollbar_h.dont_change = 1
        self.scrollbar_h.setMaximum(max(0,max_length-self.view.width()))        
        self.scrollbar_h.dont_change = 0
            
    def about(self):
        QtGui.QMessageBox.about(self, "About WhatMail",
                "<p><b>whatmail.py</b> is just a tool to play around with emails.</p>")

    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self, shortcut="Ctrl+O",
                triggered=self.open)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.aboutAct = QtGui.QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QtGui.QAction("About &Qt", self,
                triggered=QtGui.qApp.aboutQt)

    def closeEvent(self, event):
        event.accept() # let the window close
            
if __name__ == '__main__':
    
    app = QtGui.QApplication(sys.argv)
    thingy = WhatMail()
    thingy.show()
    thingy.activateWindow()
    thingy.raise_()
    
    sys.exit(app.exec_())

